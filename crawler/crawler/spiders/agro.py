import scrapy 
from ..items import CrawlerNews 

class Agro(scrapy.Spider): 
    name = 'quotes'
    start_urls = ['https://www.agrolink.com.br/']
    
    def parse (self, response):
        items = CrawlerNews()

        items['table'] = 'quotes'

        all_div_quotes = response.css('div.content-wrap div.container.clearfix div.row.clearfix')

        for quotes in all_div_quotes:
            titles = quotes.css('h4.d-none::text').extract()
            items['title'] = []
            for title in titles:
                title = title.strip()
                if len(title) > 0:
                    items['title'].append(title)
            yield items
