import scrapy 
from ..items import CrawlerProblem 

class CoffeProblem(scrapy.Spider):
    name = 'coffe_problem'
    start_urls = ['https://www.agrolink.com.br/culturas/cafe/']
    
    def parse (self, response):
        items = CrawlerProblem()

        items['table'] = 'problem'

        all_div_problem = response.css('div.row.auto-clear')[-1].css('div a div')

        for problem in all_div_problem:
            name = problem.css('h4::text').get()
            types = problem.css('label::text').get()

            items['types'] = types
            items['name'] = name
            items['culture'] = 'Café'
            
            yield items
