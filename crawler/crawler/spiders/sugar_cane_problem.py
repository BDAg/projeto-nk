import scrapy 
from ..items import CrawlerProblem 

class SugarCaneProblem(scrapy.Spider):
    name = 'sugar_cane_problem'
    start_urls = ['https://www.agrolink.com.br/culturas/cana-de-acucar/']
    
    def parse (self, response):
        items = CrawlerProblem()

        items['table'] = 'problem'

        all_div_problem = response.css('div.row.auto-clear')[-1].css('div a div')

        for problem in all_div_problem:
            name = problem.css('h4::text').get()
            types = problem.css('label::text').get()

            items['types'] = types
            items['name'] = name
            items['culture'] = 'Cana de açúcar'
            
            yield items
