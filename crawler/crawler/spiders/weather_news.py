import scrapy 
from ..items import CrawlerNewsWeather 

class WheatherNews(scrapy.Spider):
    name = 'weather_news'
    start_urls = ['https://portal.inmet.gov.br/noticias']
    
    def parse (self, response):
        items = CrawlerNewsWeather()

        items['table'] = 'weatherNews'

        all_div_news = response.css('div.col-lg-12.col-md-12.col-sm-12.col-xs-12')
        for news in all_div_news:
            title = news.css('h2.post-title::text').get()
            subtitle = news.css('h3.post-subtitle::text').get()
            
            items['title'] = title
            items['subtitle'] = subtitle
            yield items

