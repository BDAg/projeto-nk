import scrapy 
from ..items import CrawlerProblem 

class WheatProblem(scrapy.Spider):
    name = 'wheat_problem'
    start_urls = ['https://www.agrolink.com.br/culturas/trigo/']
    
    def parse (self, response):
        items = CrawlerProblem()

        items['table'] = 'problem'

        all_div_problem = response.css('div.row.auto-clear')[-1].css('div a div')

        for problem in all_div_problem:
            name = problem.css('h4::text').get()
            types = problem.css('label::text').get()

            items['types'] = types
            items['name'] = name
            items['culture'] = 'Trigo'
            
            yield items
