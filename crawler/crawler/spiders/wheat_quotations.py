import scrapy 
from ..items import CrawlerQuotations 

class WheatQuotations(scrapy.Spider):
    name = 'wheat_quotations'
    start_urls = ['https://www.agrolink.com.br/culturas/trigo/cotacoes']
    
    def parse (self, response):
        items = CrawlerQuotations()

        items['table'] = 'quotations'

        all_div_quotations = response.css('table tbody tr')

        for quotations in all_div_quotations:
            table_data = quotations.css('td::text')
            if len(table_data):
                product = table_data[0].get().replace('\n', '')
                city = table_data[2].get().replace('\n', '')
                updated_at = table_data[5].get().replace('\n', '')

                items['product'] = product
                items['city'] = city
                items['updated_at'] = updated_at
                items['culture'] = 'Trigo'
                
                yield items
