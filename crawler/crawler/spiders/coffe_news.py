import scrapy 
from ..items import CrawlerNews 

class CoffeNews(scrapy.Spider):
    name = 'coffe_news'
    start_urls = ['https://www.agrolink.com.br/culturas/cafe/noticia/lista']
    
    def parse (self, response):
        items = CrawlerNews()

        items['table'] = 'news'

        all_div_news = response.css('div.entry div.row')
        for news in all_div_news:
            title = news.css('.entry-title a::text').get()
            subtitle = news.css('.entry-content p::text').get()
            created_at = news.css('.entry-meta ul li')[1].css('small::text').get()

            items['title'] = title
            items['subtitle'] = subtitle
            items['created_at'] = created_at
            items['culture'] = 'Café'
            
            yield items
