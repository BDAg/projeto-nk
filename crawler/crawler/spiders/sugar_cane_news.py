import scrapy 
from ..items import CrawlerNews 

class SugarCaneNews(scrapy.Spider):
    name = 'sugar_cane_news'
    start_urls = ['https://www.agrolink.com.br/culturas/cana-de-acucar/noticia/lista']
    
    def parse (self, response):
        items = CrawlerNews()

        items['table'] = 'news'

        all_div_news = response.css('div.entry div.row')
        for news in all_div_news:
            title = news.css('.entry-title a::text').get()
            subtitle = news.css('.entry-content p::text').get()
            created_at = news.css('.entry-meta ul li')[1].css('small::text').get()

            items['title'] = title
            items['subtitle'] = subtitle
            items['created_at'] = created_at
            items['culture'] = 'Cana de açúcar'
            
            yield items
