import scrapy 
# from scrapy_splash import SplashRequest
from ..items import CrawlerWeather 

class CoffeNews(scrapy.Spider):
    name = 'weather'
    start_urls = ['https://g1.globo.com/previsao-do-tempo/']

    # def start_requests(self):
    #     for url in self.start_urls:
    #         yield SplashRequest(url, self.parse,
    #                             args={'wait': 1000},
    #                             )
    
    def parse (self, response):
        items = CrawlerWeather()

        items['table'] = 'weather'

        all_div_weather = response.css('div.brazil-forecast-cities.brazil-forecast-cities--first')
        #print(all_div_weather)

        
        for weather in all_div_weather:
            capital = weather.css('p.brazil-forecast-cities__header::text').get()
            data = weather.css('div.brazil-forecast-cities__temperature brazil-forecast-cities__temperature--max $0::text').get()

            items['capital'] = capital
            items['data'] = data
            #items['data'] = all_div_weather
            yield items
