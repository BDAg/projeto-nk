# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class CrawlerNews(scrapy.Item):
    title = scrapy.Field()
    subtitle = scrapy.Field()
    culture = scrapy.Field()
    table = scrapy.Field()
    created_at = scrapy.Field()

class CrawlerQuotations(scrapy.Item):
    product = scrapy.Field()
    city = scrapy.Field()
    price = scrapy.Field()
    updated_at = scrapy.Field()
    culture = scrapy.Field()
    table = scrapy.Field()
    
class CrawlerProblem(scrapy.Item):
    types = scrapy.Field()
    name = scrapy.Field()
    culture = scrapy.Field()
    table = scrapy.Field()

class CrawlerWeather(scrapy.Item):
    capital = scrapy.Field()
    data = scrapy.Field()
    table = scrapy.Field()
    
class CrawlerNewsWeather(scrapy.Item):
    title = scrapy.Field()
    subtitle = scrapy.Field()
    table = scrapy.Field()

