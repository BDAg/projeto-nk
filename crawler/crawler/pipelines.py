import pymysql
import pymysql.cursors

class CrawlerPipeline(object):
    def __init__(self):
        self.create_connection()

    def create_connection(self):
        self.connect = pymysql.connect("localhost", "root", "admin000", "nkrawler")
        self.cursor = self.connect.cursor()

    def process_item(self, item, spider):
        print(item)
        print('================================')
        self.store_db(item)
        return item

    def store_db(self, item):
        if (item['table'] == 'quotes'):
            if len(item['title']):
                values = ''
                count = 0
                for title in item['title']:
                    count += 1

                    print(title)

                    if count == len(item['title']):
                        values = str(values + "('" + title + "')")
                        continue
                    
                    values = str(values + "('" + title + "'), ")
                
                query = str("INSERT INTO news(title) VALUES " + values)

                print(query)

                self.cursor.execute(query)

                self.connect.commit()


        if (item['table'] == 'news'):
            if (item['title'] and item['created_at']):
                title = item['title']
                subtitle = item['subtitle']
                created_at = item['created_at']
                culture = item['culture']

                self.cursor.execute("INSERT INTO news (title, subtitle, created_at, culture) VALUES (%s, %s, %s, %s)",
                                    (title, subtitle, created_at, culture))
                self.connect.commit()

        if (item['table'] == 'quotations'):
            if (item['product']):
                product = item['product']
                city = item['city']
                updated_at = item['updated_at']
                culture = item['culture']

                self.cursor.execute("INSERT INTO quotations (product, city, updated_at, culture) VALUES (%s, %s, %s, %s)",
                                    (product, city, updated_at, culture))
                self.connect.commit()

        if (item['table'] == 'problem'):
            if (item['name']):
                types = item['types']
                name = item['name']
                culture = item['culture']

                self.cursor.execute("INSERT INTO problem (type, name, culture) VALUES (%s, %s, %s)",
                                    (types, name, culture))
                self.connect.commit()
        
        if (item['table'] == 'weatherNews'):
            if (item['title']):
                title = item['title']
                subtitle = item['subtitle']

                self.cursor.execute("INSERT INTO weatherNews (title, subtitle) VALUES (%s, %s)",
                                    (title, subtitle))
                self.connect.commit()

  #      if (item['table'] == 'weather'):
            #if (item['capital']):
            #    capital = item['capital']
            #    data = item['data']
#
#                self.cursor.execute("INSERT INTO weather (capital, data) VALUES (%s, %s)",
#                                    (capital, data))
#              self.connect.commit()

        