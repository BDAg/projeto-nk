CREATE DATABASE Nkrawler;

USE Nkrawler;

CREATE TABLE principal (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title TEXT,
  subtitle TEXT,
  culture VARCHAR (255),
  created_at VARCHAR (20)
);

CREATE TABLE news (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title TEXT,
  subtitle TEXT,
  culture VARCHAR (255),
  created_at VARCHAR (20)
);

CREATE TABLE quotations (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  product VARCHAR (255),
  city VARCHAR (255),
  price FLOAT,
  updated_at VARCHAR (20),
  culture VARCHAR (255)
);

CREATE TABLE problem (
  id int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  type VARCHAR (255),
  name VARCHAR (255),
  culture VARCHAR (255)
);
